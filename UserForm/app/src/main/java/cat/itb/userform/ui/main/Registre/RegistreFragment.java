package cat.itb.userform.ui.main.Registre;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class RegistreFragment extends Fragment {

    @BindView(R.id.LoginInRegistre)
    Button LoginInRegistre;
    @BindView(R.id.RegistreinRegistre)
    Button RegistreinRegistre;
    @BindView(R.id.login)
    TextView login;
    @BindView(R.id.name)
    TextInputLayout name;
    @BindView(R.id.password)
    TextInputLayout password;
    @BindView(R.id.repeatpassoword)
    TextInputLayout repeatpassoword;
    @BindView(R.id.mail)
    TextInputLayout mail;
    EditText gender;
    EditText birthdate;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    private RegistreViewModel mViewModel;

    public static RegistreFragment newInstance() {
        return new RegistreFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registre_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegistreViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        birthdate = getView().findViewById(R.id.birthdateSelect);
        gender= getView().findViewById(R.id.genderPro);
        birthdate.setOnClickListener(this::onClick);
        gender.setOnClickListener(this::onClick);
    }

    private void onClick(View view) {
          if (view.getId() == R.id.birthdateSelect) {
                showCalendar();
            } else if (view.getId() == R.id.genderPro) {
                showOptions();
            }
        }


    @OnClick({R.id.LoginInRegistre, R.id.RegistreinRegistre})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.LoginInRegistre:
                if (valida()) {
                    Navigation.findNavController(getView()).navigate(R.id.loginFragment);
                }
                break;
            case R.id.RegistreinRegistre:
                Navigation.findNavController(getView()).navigate(R.id.userLoggedFragment);

                break;
            case R.id.genderPro:
                showOptions();
                break;
            case R.id.birthdateSelect:
                showCalendar();
                break;

        }
    }

    public boolean valida() {
        if (name.getEditText().getText().toString().isEmpty()) {
            name.setError("Error Name");
            scrollTo(name);
            return false;
        } else if (password.getEditText().getText().toString().isEmpty()) {
            password.setError("Error Password");
            scrollTo(password);
            return false;
        } else if (!password.getEditText().getText().toString().equals(repeatpassoword.getEditText().getText().toString())) {
            repeatpassoword.setError("Error Name");
            scrollTo(repeatpassoword);
            return false;
        } else if (mail.getEditText().getText().toString().isEmpty()) {
            mail.setError("Error Password");
            scrollTo(mail);
            return false;
        }
        return true;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }

    private void showOptions() {
        String [] genderOptions = {getString(R.string.gender_female),
                getString(R.string.gender_male)};

        new MaterialAlertDialogBuilder(getContext())
                .setTitle(R.string.title_gender)
                .setItems(genderOptions, /* listener */ new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        gender.setText(genderOptions[which]);
                    }
                })
                .show();
    }
    private void showCalendar() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(R.string.title_calendar);
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    private void doOnDateSelected(Long aLong) {
        String pattern = "dd/MM/yyyy";
        DateFormat df = new SimpleDateFormat(pattern);
        Date date = new Date(aLong);
        birthdate.setText(df.format(date));
    }


}

