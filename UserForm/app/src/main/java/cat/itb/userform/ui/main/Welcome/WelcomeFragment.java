package cat.itb.userform.ui.main.Welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.ui.main.Welcome.WelcomeViewModel;

public class WelcomeFragment extends Fragment {


    @BindView(R.id.login)
    Button login;
    @BindView(R.id.registre)
    Button registre;
    private WelcomeViewModel mViewModel;

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WelcomeViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @OnClick({R.id.login, R.id.registre})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                Navigation.findNavController(getView()).navigate(R.id.loginFragment);

                break;
            case R.id.registre:
                Navigation.findNavController(getView()).navigate(R.id.registreFragment);

                break;
        }
    }
}
