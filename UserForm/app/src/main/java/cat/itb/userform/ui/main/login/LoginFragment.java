package cat.itb.userform.ui.main.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.ui.main.login.LoginViewModel;

public class LoginFragment extends Fragment {

    @BindView(R.id.LoginInLogin)
    Button LoginInLogin;
    @BindView(R.id.RegistreInLogin)
    Button RegistreInLogin;
    @BindView(R.id.loginName)
    TextInputLayout loginName;
    @BindView(R.id.loginPass)
    TextInputLayout loginPass;

    Button btnLogin, btnRegister;

    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

        mViewModel.loading.observe(this, this::loadingObserver);
        mViewModel.error.observe(this, this::errorObserve);
        mViewModel.logged.observe(this, this::loggedObserve);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        btnLogin = getView().findViewById(R.id.LoginInLogin);
        btnRegister = getView().findViewById(R.id.LoginInRegistre);

    }



    @OnClick({R.id.LoginInLogin, R.id.RegistreInLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.LoginInLogin:

                if (valida()) {
                    mViewModel.login(loginName.getEditText().toString(), loginPass.getEditText().toString());
                    Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_userLoggedFragment);

                }
                break;
            case R.id.RegistreInLogin:
                Navigation.findNavController(getView()).navigate(R.id.registreFragment);


                break;
        }
    }
    ProgressDialog dialog;
    private void loadingObserver(Boolean bool) {

        if (bool) {
            dialog = ProgressDialog.show(getContext(), getString(R.string.loading), "...", true);
            dialog.show();
        } else {
            if(dialog!=null)
                dialog.dismiss();
        }
    }

    private void loggedObserve(Boolean logged) {
        if (logged) {
            Navigation.findNavController(getView()).navigate(R.id.action_loginFragment_to_userLoggedFragment);
        }
    }

    private void errorObserve(String error) {
        if (error!=null) {
            Snackbar.make(getView(), error, Snackbar.LENGTH_SHORT)
                    .show();
        }
    }
    private boolean valida() {
        if (loginName.getEditText().getText().toString().isEmpty()) {
            loginName.setError("Error Name");
            return false;
        } else if (loginPass.getEditText().getText().toString().isEmpty()) {
            loginPass.setError("Error Password");
            return false;
        }
        return true;


    }
}
