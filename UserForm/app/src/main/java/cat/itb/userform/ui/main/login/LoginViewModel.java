package cat.itb.userform.ui.main.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.userform.ui.main.API.UserApi;
import cat.itb.userform.ui.main.API.UserApiProvider;
import cat.itb.userform.ui.main.API.UserSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {
    MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    MutableLiveData<String> error = new MutableLiveData<>();
    UserApi userApi = UserApiProvider.getUserApi();

    public void login(String username, String password) {
        loading.setValue(true);

        userApi.login(username).enqueue(new Callback<UserSession>() {
            @Override
            public void onResponse(Call<UserSession> call, Response<UserSession> response) {
                UserSession userSession = response.body();
                if (userSession.isLogged()) {
                    loading.setValue(false);
                    logged.setValue(true);
                } else {
                    loading.setValue(false);
                    error.setValue(userSession.getError());
                }
            }

            @Override
            public void onFailure(Call<UserSession> call, Throwable t) {
                loading.setValue(false);
                error.setValue("Connection failed");
            }
        });
    }
}

