package cat.itb.userform;

import android.app.Activity;
import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("cat.itb.userform", appContext.getPackageName());
    }
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void Welcome() {
        // test code here

        onView(withId(R.id.login)).perform(click());
        onView(withId(R.id.userLoggedFragment)).check(matches(isDisplayed()));

        onView(withId(R.id.registre)).perform(click());
        onView(withId(R.id.registreFragment)).check(matches(isDisplayed()));


    }
    public  void Logins(){
        onView(withId(R.id.loginName))
                .perform(replaceText("user1"));
        onView(withId(R.id.loginPass))
                .perform(replaceText("Password1"));
        onView(withId(R.id.LoginInLogin)).perform(click());
        onView(withId(R.id.userLoggedFragment)).check(matches(isDisplayed()));
    }
        public void LoginsBad() {

            onView(withId(R.id.login)).perform(click());
            onView(withId(R.id.loginName))
                    .perform(replaceText("use1"));
            onView(withId(R.id.loginPass))
                    .perform(replaceText("Pass"));
            onView(withId(R.id.LoginInLogin)).perform(click());

            onView(withId(R.id.userLoggedFragment)).check(matches(isDisplayed()));
        }

    @Test
    public void RegistreTest(){
        onView(withId(R.id.registre)).perform(click());
        onView(withId(R.id.nameRe)).perform(replaceText("user"));
        onView(withId(R.id.passRe)).perform(replaceText("pass"));
        onView(withId(R.id.RepassRe)).perform(replaceText("pass"));
        onView(withId(R.id.emailRe)).perform(replaceText("patat@pata.pata"));
        onView(withId(R.id.RegistreinRegistre)).perform(click());

    }
    @Test
    public void RegistreTestBad(){
        onView(withId(R.id.registre)).perform(click());
        onView(withId(R.id.nameRe)).perform(replaceText("user"));
        onView(withId(R.id.passRe)).perform(replaceText("pass"));
        onView(withId(R.id.RepassRe)).perform(replaceText("ps"));
        onView(withId(R.id.emailRe)).perform(replaceText("patat@pata.pata"));
        onView(withId(R.id.RegistreinRegistre)).perform(click());

    }
}
